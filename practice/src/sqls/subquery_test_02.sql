/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(t.orderCount) AS minOrderItemCount,
    MAX(t.orderCount) AS maxOrderItemCount,
    round(AVG(t.orderCount)) AS avgOrderItemCount
    from (
        select count(od.orderNumber) as orderCount from orderdetails od
        where od.orderNumber in (select o.orderNumber from orders o)
        group by od.orderNumber
    ) t;