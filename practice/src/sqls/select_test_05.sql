/*
 * 请告诉我所有具备 `state` 的顾客（customer）中的 state 的可能值（不重复）。结果请按照 state
 * 排序。
 */
select distinct state from customers where state is not null order by state;