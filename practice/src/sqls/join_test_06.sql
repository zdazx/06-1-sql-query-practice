/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
SELECT DISTINCT CONCAT(e1.lastName, ', ', e1.firstName) AS employee,
       CASE
       WHEN e1.reportsTo IS NULL THEN '(Top Manager)'
       ELSE CONCAT(e2.lastName, ', ', e2.firstName)
       END AS manager
	FROM employees e1
	JOIN employees e2
    ON e1.reportsTo IS null
    OR e1.reportsTo = e2.employeeNumber
    ORDER BY manager, employee;